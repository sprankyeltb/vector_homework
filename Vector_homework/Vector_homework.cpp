﻿#include <iostream>
#include <cmath>
using namespace std;

class Vector
{
private:
	double x;
	double y;
	double z;
public:
	// 1.Конструктор по умолчанию
	Vector() : x(0), y(0), z(0)
	{}

	// 2.Конструктор с инициализацией переменныx
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	// 3.устанавливает значения переменных X,Y,Z
	void SetXYZ(double newX, double newY, double newZ)
	{
		x = newX;
		y = newY;
		z = newZ;
	}

	//4.возвращает значения переменных X,Y,Z
	double GetXYZ()
	{
		cout << x << " " << y << " " << z << endl << endl;
		return 0;
	}

	// 5.Расчет модуля (длины вектора)
	void VectorLength()
	{
		//формула: L = корень (a1*x^2) + (a2*y^2) + (a3*z^2) 
		double a1 = (x * x);
		double a2 = (y * y);
		double a3 = (z * z);
		double L = a1 + a2 + a3;
		cout << "Vecor legth = " << sqrt(L) << endl; //sqrt возвращает квадратный корень
	}
};

int main()
{
	Vector v;
	v.SetXYZ(10, 10, 10);
	v.GetXYZ();
	v.VectorLength();

	return 0;
}